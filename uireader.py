from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtCore import QTimer
import pyqtgraph as pg
import random

class GraphSlide:
    def __init__(self, inform):
        self.mainui = inform
        self.mainui.show()

class TheSlider:
    def __init__(self, inform):
        self.mainui = inform

        self.mainui.vsliderVal.valueChanged.connect(self.labelshow)

    def labelshow(self):
        value = self.mainui.vsliderVal.value()
        print(value)
        self.mainui.vsliderVal.setRange(0, 100)
        self.mainui.lblChartNum.setText(str(value))
        return value

class TheGraph(TheSlider):
    def __init__(self, inform):
        self.mainui = inform

        self.plot = pg.PlotWidget()
        self.mainui.qvboxChart.addWidget(self.plot)
        self.power = [1]
        self.time_counter = 0

        self.timer = QTimer()
        self.timer.timeout.connect(self.trend_chart)
        self.timer.start(1000)

        self.trend_chart()

    def trend_chart(self):
        pfrange = TheGraph.labelshow(self)
        self.time_counter += 1

        impedance = random.uniform(0, pfrange)
        self.power.append(impedance)

        we = len(self.power) - 1

        if len(self.power) == 30:
            self.plot.clear()
            self.power = [self.power[we]]

        self.plot.plot(self.power, pen='g', symbol='o', symbolSize=5, symbolBrush='w', name='Prices')

if __name__ == '__main__':
    app = QApplication([])
    ui = loadUi('slidergraph.ui')
    main = GraphSlide(ui)
    graph = TheGraph(ui)
    slider = TheSlider(ui)
    app.exec()